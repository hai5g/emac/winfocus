;;; winfocus.el --- auto-focus to newly displayed buffer  -*- lexical-binding: t; -*-

;; Copyright (C) Cong Hai Nguyen

;; Author: Cong Hai Nguyen; <congchieng9@gmail.com>
;; Keywords: convenience

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;
;;; * Introduction
;;;
;;; Emacs popup windows does not get focus so to close them a user
;;; needs to use some key combination (e.g. C-x 1
;;; (`delete-other-windows') or `winner-undo').
;;;
;;; If newly displayed window gets selected, user can just press 1 key
;;; "q" to quit (for special-mode windows) or they can traverse the
;;; newly created window since it has gotten selected automatically.
;;;
;;; This library aims to solve that problem.
;;;
;;; * Design
;;;
;;; ** datatstructure
;;; - last window list, last window buffer list
;;; - this window list, this window buffer list
;;;
;;; ** algorithm
;;;
;;; The code below based on 2 hooks working in tandem in Emacs when
;;; windows change (popup, switch to another buffer, etc):
;;; - `display-buffer-alist': happens when `display-buffer' is called
;;; - `window-configuration-change-hook': happens when new window
;;;   created or deleted
;;;
;;; The procedures are like this:
;;; - compare the current list of live windows with that from the most
;;;   recent window-configuration change to determine if new windows
;;;   created/deleted/the same, then
;;; - compare the current list of buffers (in the same order as their
;;;   appearance in the live windows) with that of the most recent
;;;   window-configuration change to determine whether a buffer has
;;;   been changed in a window
;;; - use the most recent buffer accessed by `display-buffer' to find
;;;   which window a buffer has been "redisplayed" so as to switch to
;;;   it
;;;
;;; * Similar packages
;;;
;;; - `popwin.el' has more features and options, but somehow when a
;;;   live "*Help*" window refreshed (i.e. it changes its content to
;;;   display some other Help content), the focus is switched to the
;;;   other window.
;;;

(require 'cl-seq)


(defvar h-window-list-last nil
  "Used for smart switching to a popup window.")

(defvar h-window-buffer-list-last nil
  "Used for smart switching to a popup window.")


(defmacro h-window-message (fmt &rest msg)
  "Use to toggle printing debugging messages."
  nil
  `(if nil
       (message ,fmt ,@msg)))


(defun h-window-log-message (title)
 "Print debug messages."
 (h-window-message "[%s] %s: %s"
	(h-time-printf "%H:%M:%S:%N")
	   title
	   (window-list)))


(defun h-select-next-window ()
  "Convenience function that selects the next window."
  (select-window (next-window)))

(defun h-select-window-buffer (buf-or-name)
  "Select the window that contains the BUFFER-OR-NAME"
  (let ((w (get-buffer-window buf-or-name)))
    (if w
	(select-window w)
      (error "no window found for buffer %s" buf-or-name))))


(defun winfocus--main ()
  (unless (minibufferp) ; don't switch if currently in minibuffer
					; e.g. when the completion window pops up
    (let* ((l2 (window-list))
	   (l1 h-window-list-last)
	   (l2bl nil)
	   (l1bl h-window-buffer-list-last)
	   (l2l (length l2))
	   (l1l (length l1))
	   (l2w (car l2))
	   (l1w (car l1))
	   (l2b (window-buffer l2w))
	   (l1b (window-buffer l1w))
	   (l1c  ; elements unique to l1
	    (cl-set-difference l1 l2))
	   (l2c ; elements unique to l2
	    (cl-set-difference l2 l1))
	   (lc (cl-union l1c l2c))
	   )
      (dolist (w l2)
	(setq l2bl (cons (window-buffer w) l2bl)))
      (setq l2bl (reverse l2bl))
      (h-window-log-message "WIN:")
      (cond
       ((< l1l l2l)
	(h-window-message "\tWIN new window created: %s" l2c)
	(h-select-next-window))
       ((> l1l l2l)
	(h-window-message "\tWIN windows deleted: %s" lc))
       ((eq l1 l2)
	(h-window-message "\tWIN: same windows, buffers changed!?"))
       ((equal (cl-set-exclusive-or l1 l2) nil)
	(let* ((l1u  ; elements unique to the first list
		(cl-set-difference l1bl l2bl))
	       (l2u ; elements unique to the second list
		(cl-set-difference l2bl l1bl))
	       (lc (cl-union l1u l2u))
	       )
	  (pcase (length l2u)
	    (0 (h-window-message "\tWIN: same windows and buffers, buffer content refreshed"))
	    (1 (if l1u
		   (progn
		     (h-window-message "\tWIN: same windows, buffer switched: %s" (car l2u))
		     (h-select-window-buffer (car l2u)))
		 (if (null lc)
		     (progn
		       (h-window-message "\tWIN: same windows, a buffer reverted"))
		   (progn
		     (h-window-message "\tWIN: window replaced (buffer %s)" lc)
		     (h-select-window-buffer (car lc))
		     ))))
	    (_ (h-window-message "\tWIN: same windows, SEVERAL buffers changed %s" l2u)))))
       (t
	(h-window-message "\tWIN: changed windows")))
      (setq h-window-list-last l2
	    h-window-buffer-list-last l2bl))))


(defvar winfocus-last-display-buffer nil
  "Store the buffer to be displayed by display-buffer.")


(defun winfocus--display-buffer-function (b l)
  "B: the buffer to display-buffer-alist
L: the alist of the same form as `display-buffer' alist."
  (h-window-message "BUF: %s, WIN: %s, LIST: %s" b (selected-window) l)
  (setq winfocus-last-display-buffer b)
  nil ; indicate display-buffer keep trying other function after this
      ; function
)


(defun h-display-buffer-focus-buffer ()
  "Select the newly displayed buffer like '*grep*', '*Help*', etc."
  (h-select-window-buffer winfocus-last-display-buffer))


(define-minor-mode winfocus-mode
  "A minor mode for auto-focusing on newly displayed window."
  :init-value nil
  :lighter nil
  :keymap nil
  :global t
  (if winfocus-mode
      (progn
	(add-hook 'window-configuration-change-hook 'winfocus--main)
	(add-to-list
	 'display-buffer-alist
	 (cons 'winfocus--display-buffer-function '(nil))))
    (progn
      (remove-hook 'window-configuration-change-hook 'winfocus--main)
      (setq display-buffer-alist
	    (remove display-buffer-alist
		    (cons 'winfocus--display-buffer-function '(nil)))))))


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(provide 'winfocus)
;;; winfocus.el ends here